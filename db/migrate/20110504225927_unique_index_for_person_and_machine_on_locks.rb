class UniqueIndexForPersonAndMachineOnLocks < ActiveRecord::Migration
  def self.up
    add_index :locks, [:person_id, :machine_id], :unique => true
  end

  def self.down
    remove_index :locks, :column => [:person_id, :machine_id]
  end
end