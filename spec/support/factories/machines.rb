# Read about factories at http://github.com/thoughtbot/factory_girl

Factory.define :machine do |f|
  f.sequence(:name) {|n| "Machine #{n}" }
end
