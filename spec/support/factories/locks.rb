# Read about factories at http://github.com/thoughtbot/factory_girl

Factory.define :lock do |f|
  f.association :person, :factory => :person
  f.association :machine
  f.sequence(:done_at) {|n| n.hours.from_now }
end
