require 'spec_helper'

describe Machine do
  it { should have_one(:lock).dependent(:destroy) }

  it { should validate_presence_of(:name) }
  it { Factory.create(:machine) and should validate_uniqueness_of(:name) }
end
