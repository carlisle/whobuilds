require 'spec_helper'

describe Lock do
  it { should belong_to(:person) }
  it { should belong_to(:machine) }
  
  context "with locked Machine" do
    it "should lengthen lock time by 30 minutes when extended" do
      lock = Factory.create(:lock)
      orig_done_at = lock.done_at
      lock.extend_time(30)
      extended_done_at = lock.done_at
      orig_done_at.should be < extended_done_at
      orig_done_at.should be == extended_done_at.ago(30 * 60)
    end
  end
end
