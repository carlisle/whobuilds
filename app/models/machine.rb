class Machine < ActiveRecord::Base
  has_one :lock, :dependent => :destroy
  
  validates :name, :presence => true, :uniqueness => true
  
  scope :available, joins('LEFT OUTER JOIN locks l ON machines.id = l.machine_id').where(:l => {:id => nil})
end
