class MachinesController < ApplicationController
  inherit_resources
  
  # create a 2 hour lock for current user on given machine
  # def lock
  #   machine = Machine.find(params[:id])
  #   if machine && machine.lock.nil?
  #     lock = machine.build_lock(:person => current_person, :done_at => 2.hours.from_now)
  # 
  #     respond_to do |wants|
  #       if lock.save
  #         flash[:notice] = "It's all yours. Happy building."
  #         wants.html { redirect_to(locks_path) }
  #         wants.xml  { render :xml => lock, :status => :created, :location => lock }
  #       else
  #         wants.html { render :action => "index" }
  #         wants.xml  { render :xml => lock.errors, :status => :unprocessable_entity }
  #       end
  #     end
  #   else
  #     redirect_to(locks_path)
  #   end
  # end

end
