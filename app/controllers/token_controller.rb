class TokenController < ApplicationController

  def reset
    current_person.reset_authentication_token!
    redirect_to :back, :notice => "Authentication token has been reset. Don't forget to update your links."
  end

end
