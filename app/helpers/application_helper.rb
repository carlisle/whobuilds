module ApplicationHelper
  def page_heading
    @page_heading || "#{controller_name.humanize} #{action_name.humanize}"
  end
  
  def available_machines_sidebar
    render 'layouts/available_machines_sidebar', :machines => Machine.available unless Machine.available.count.blank?
  end
end
